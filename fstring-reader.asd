;;;; This code is released to the Public Domain by Kieran Grant
;;;; Failing that, see the CC0 at https://creativecommons.org/publicdomain/zero/1.0/

(defsystem "fstring-reader"
    :author "Kieran Grant"
    :license "Public Domain"
    :version "0.0.2"
    :depends-on ("named-readtables")
    :serial t
    :components
    ((:file "package")
     (:file "fstring-reader")))
