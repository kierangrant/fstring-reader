;;;; This code is released to the Public Domain by Kieran Grant
;;;; Failing that, see the CC0 at https://creativecommons.org/publicdomain/zero/1.0/

(in-package :fstring-reader)

#|
fstream-reader

Read in a form like $"prefix~!code!postfix"
that translates into
(format nil "prefix~Apostfix" code)
replacing every occrance of ~!...! in the string with ~A
(except if character before the first '!' is ':') and move code, in order to after string in format form.

It uses an otherwise invalid format directive "~!" with the optional paramter ":" to supress output of
"~A". It passes all other characters to the string verbaim, if it sees any format directive OTHER then
"~!" or "~:!" it passes it verbatim.

You can have whitespace before and after the expression in-between the two "!"'s
$"Hello ~! name !, you are ~:! age !~D years old~%"
Becomes
(FORMAT NIL "Hello ~A, you are ~D years old~%" NAME AGE)
|#

(defun fstring-reader (stream char)
  (declare (ignore char))
  (if (not (char= (peek-char nil stream t nil t) #\"))
      (error "'$' formatter string must be followed by a '\"'")
      (read-char stream t nil t))	; discard the opening '"'
  (let ((s (make-array 0 :element-type 'character :fill-pointer 0 :adjustable t)) (forms))
    (loop for char = (read-char stream t nil t) do
	 (cond
	   ;; Escaping ALWAYS has highest priority
	   ((char= char #\\)
	    (vector-push-extend (read-char stream t nil t) s))
	   ;; If we see end of string, TER-MIN-ATE!!!
	   ((char= char #\")
	    (loop-finish))
	   ;; Fun times, if we see a "~" and it follows, optionally by a ":" and THEN "!", do awesome magic
	   ;; otherwise we need to unread the ":" and the "~"... (we error out the moment we see something we
	   ;; don't recognise, which means "~:@!.." will backup on seeing "@" and ignore it...)
	   ((char= char #\~)
	    (let ((peek (read-char stream t nil t)) peek2)
	      (labels
		  ((never-read (stream char)
		     (declare (ignore stream char))
		     (error "'!' is set to a terminating macro character in $\"\" forms, please enclose in '||' form"))
		   (read-sub-form (inhibit-format-string)
		     (if (not inhibit-format-string)
			 (progn
			   (vector-push-extend #\~ s)
			   (vector-push-extend #\A s)))
		     (let ((*readtable* (copy-readtable)))
		       (set-macro-character #\! #'never-read nil) ; we don't actually READ the "!", we just skip it
		       (setf forms (nconc forms (list (read stream t nil t))))
		       (if (char= (peek-char t stream t nil t) #\!)
			   (read-char stream t nil t)
			   (error "Missing finilizer '!' character")))))
		(cond
		  ((char= peek #\:)
		   ;; We have seen "~:" so far... we need to call (read-char) so we can again peek-char...
		   (setf peek2 (read-char stream t nil t))
		   (if (char= peek2 #\!)
		       (read-sub-form t)
		       (progn
			 ;; bugger
			 (vector-push-extend char s)
			 (vector-push-extend peek s)
			 (vector-push-extend peek2 s))))
		  ((char= peek #\!)
		   ;; we have seen "~!" so far, we are a good to go
		   (read-sub-form nil))
		  (t
		   ;; nope
		   (vector-push-extend char s)
		   (vector-push-extend peek s))))))
	   ;; Else, stash the char!
	   (t (vector-push-extend char s))))
    `(format nil ,(coerce s 'simple-string) ,@forms)))

(named-readtables:defreadtable fstring-reader
  (:merge :standard)
  (:macro-char #\$ #'fstring-reader t))
