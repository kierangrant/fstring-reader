;;;; This code is released to the Public Domain by Kieran Grant
;;;; Failing that, see the CC0 at https://creativecommons.org/publicdomain/zero/1.0/

(defpackage :fstring-reader
  (:use :cl)
  (:export #:fstring-reader))
